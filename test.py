# from sqlalchemy import create_engine
# from sqlalchemy.orm import sessionmaker
# from starlette.testclient import TestClient
#
# from main import Base, app, get_db
#
# SQLALCHEMY_DATABASE_URL = "sqlite:///./test.db"
#
# engine = create_engine(
#     SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
# )
# TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
# Base.metadata.create_all(bind=engine)
#
#
# def override_get_db():
#     db = TestingSessionLocal()
#     try:
#         yield db
#     finally:
#         db.close()
#
#
# # THIS
# app.dependency_overrides[get_db] = override_get_db
# # THIS
#
# client = TestClient(app)
#
#
# def test_create_user():
#     response = client.post(
#         "/users/",
#         json={"email": "bob@builder.com"},
#     )
#     assert response.status_code == 200